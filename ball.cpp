/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>

Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }
//Novo método construtor que permite o usuario determinar a posicao inicial da bola
Ball::Ball(double xp, double yp){
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
  	std::cout << "Invalid position in x: " << xp << std::endl;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
  } else {
  	std::cout << "Invalid position in y: " << yp << std::endl;
  }
}

void Ball::step(double dt)
{
//Calculates the new position in x and y
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}

void Ball::display()
{
  std::cout<<x<<" "<<y<<std::endl ;
}

//Metodos acessores - Task 5/6
void Ball::set(double xp, double yp){
	
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
  	std::cout << "Invalid position in x: " << xp << std::endl;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
  } else {
  	std::cout << "Invalid position in y: " << yp << std::endl;
  }

} 
double Ball::getx()
{
 	return x; 
}
double Ball::gety()
{
	return y; 
}
